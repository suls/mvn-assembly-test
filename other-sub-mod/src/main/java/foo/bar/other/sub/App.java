package foo.bar.other.sub;

import com.google.common.eventbus.EventBus;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        EventBus e = new EventBus();
        e.post(null);
    }
}
