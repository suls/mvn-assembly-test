# Objective

To bundle multi-module maven project in single distribution.

Run maven:

```
% mvn clean package
```

look at what was generated:

## sub-mod

Ok! Depends on `guava-14.0.1.jar`

```
% unzip -p distribution/target/distribution-1.0-SNAPSHOT-bin/sub-mod-1.0-SNAPSHOT.jar META-INF/MANIFEST.MF
Manifest-Version: 1.0
Built-By: mathias.sulser
Build-Jdk: 1.6.0_65
Class-Path: guava-14.0.1.jar
Created-By: Apache Maven 3.2.3
Main-Class: foo.bar.sub.App
Archiver-Version: Plexus Archiver
```

## other-sub-mod

Ok! Depends on `guava-18.0.jar`

```
% unzip -p distribution/target/distribution-1.0-SNAPSHOT-bin/other-sub-mod-1.0-SNAPSHOT.jar META-INF/MANIFEST.MF
Manifest-Version: 1.0
Built-By: mathias.sulser
Build-Jdk: 1.6.0_65
Class-Path: guava-18.0.jar
Created-By: Apache Maven 3.2.3
Main-Class: foo.bar.other.sub.App
Archiver-Version: Plexus Archiver
```

## distribution

```
% tree distribution/target/distribution-1.0-SNAPSHOT-bin/
distribution/target/distribution-1.0-SNAPSHOT-bin/
├── guava-14.0.1.jar
├── other-sub-mod-1.0-SNAPSHOT.jar
└── sub-mod-1.0-SNAPSHOT.jar
```

Houston, we have a problem! 
`guava-18.0.jar` is missing!

# Next steps
Help welcome!